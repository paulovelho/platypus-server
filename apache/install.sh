#! /bin/bash

sed -i -e "s/\[VIRTUALHOST\]/$VIRTUALHOST/g" /etc/apache2/sites-available/platypusweb.conf

a2enmod proxy
a2enmod proxy_fcgi
a2enconf servername
a2ensite platypusweb

/usr/sbin/apache2ctl -D FOREGROUND
